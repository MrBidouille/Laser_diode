## Découpe laser diode

PROUT

Par Monsieur Bidouille

[Youtube](https://www.youtube.com/user/monsieurbidouille)

[Facebook](https://www.facebook.com/monsieurbidouille)

[Twitter](https://twitter.com/MrBidouille)

[Mastodon](https://framapiaf.org/web/accounts/23449)

[Site web](http://monsieurbidouille.fr)

[Discord](https://discord.gg/93BVEz6)

Vous trouverez les plans en dessin vectoriel (format AI ou SVG) pour découper le châssis de la laser diode à la découpe laser.
La modélisation 3D (Solidworks) est aussi disponible.

**Je ne vais pas forcément soutenir cette machine, n'attendez pas des MAJ régulières. Je partage cette machine pour ceux qui veulent - comme moi - en construire une "pour voir"  ou pour ceux qui voudraient partir sur cette base pour l'améliorer.**

## Documentation
Regardez la page [wiki](https://framagit.org/MrBidouille/Laser_diode/wikis/home) du projet

## Licence
[GNU GPLV3](https://www.gnu.org/licenses/gpl-3.0.html)

## Reglementation
/!\ Attention, il est interdit de détenir un laser de classe supérieure à 2 pour les non professionnels. Source : [ici](https://www.economie.gouv.fr/dgccrf/securite-des-appareils-a-laser-sortant-nouvelle-reglementation-a-partir-1er-juillet-2013) ou [la](http://www.france-airsoft.fr/forum/index.php?showtopic=191540)

## Sécurité
L'utilisation d'un laser diode de classe supérieure à 2 demande des protections spécifiques, notamment pour les yeux. Il existe des lunettes spéciales. Faites attention à ce que la protection soit prévue pour la longueur d'onde de votre laser.